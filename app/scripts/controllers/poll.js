'use strict';
/**
 * @ngdoc function
 * @name pollyApp.controller:ChatCtrl
 * @description
 * # ChatCtrl
 * A demo of using AngularFire to manage a synchronized list.
 */
angular.module('pollyApp')
  .controller('PollCtrl', function ($scope, Ref, FBURL, $firebaseArray, $timeout, $routeParams, $firebaseObject, $location, $rootScope) {
    $scope.loaded = false;
    $scope.questions = $firebaseArray(Ref.child('questions'));

    // display any errors
    $scope.questions.$loaded().catch(alert);


    $scope.questionId = $routeParams.questionId;
    var r = new Firebase(FBURL + '/questions/' + $scope.questionId);

    $scope.question = $firebaseObject(r);
    $scope.question.$loaded(function() {
      $scope.loaded = true;
      if ($scope.question.$value === null) {
        $location.path('/');
      }
      $timeout(function(){
        angular.element('body').addClass('animate');
      }, 100);
    });

    $scope.answerQuestion = function(ananswer) {
      ananswer.count = ananswer.count + 1;
      $scope.question.$save(ananswer).then(function(ref) {
        $location.path('/' + $scope.question.$id + '/results');
      });
    };

    $scope.getTotal = function() {
      var total = 0;
      for (var i = 0; i < $scope.question.answers.length; i++) {
        total += $scope.question.answers[i].count;
      };
      return total;
    };

  });
