'use strict';

angular.module('pollyApp')
.controller('MainCtrl', function ($scope, Ref, $firebaseArray, FBURL, $timeout, $routeParams, $location) {

	var Q_LOCATION = FBURL + '/questions';
	$scope.questions = $firebaseArray(Ref.child('questions'));

	var hashids = new Hashids("seeonsoolanePoll", 5, "abcdefghijklmnopqrstuvwxyz1234567890");

	$scope.newQuestion = {
		text: '',
		answers: [
			{
				text: ''
			}
		]
	};

	$scope.addQuestion = function() {
		var date = new Date();
		var qId = hashids.encode(parseInt(date.getMilliseconds() + "" + $scope.questions.length));
		$scope.newQuestion["createdAt"] = Firebase.ServerValue.TIMESTAMP;

		var qRef = new Firebase(Q_LOCATION);
		qRef.child(qId).set($scope.newQuestion, function() {
			$location.path('/' + qId + '/results');
		});

		$scope.newQuestion = {
			text: '',
			answers: [
				{
					text: ''
				}
			]
		};
	};

	$scope.focusLastAnswer = function () {
		$timeout(function() {
			$('.form-control').last().focus();
		}, 0);
	};
});