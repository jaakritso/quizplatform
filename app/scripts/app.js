'use strict';

/**
 * @ngdoc overview
 * @name pollyApp
 * @description
 * # pollyApp
 *
 * Main module of the application.
 */
angular.module('pollyApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'firebase',
    'firebase.ref'
  ]);
