'use strict';
/**
 * @ngdoc overview
 * @name pollyApp:routes
 * @description
 * # routes.js
 *
 * Configure routes for use with Angular, and apply authentication security
 */
angular.module('pollyApp')

  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/:questionId', {
        templateUrl: 'views/answer.html',
        controller: 'PollCtrl'
      })
      .when('/:questionId/results', {
        templateUrl: 'views/chart.html',
        controller: 'PollCtrl'
      })
      .otherwise({redirectTo: '/'});
      
  }]);
